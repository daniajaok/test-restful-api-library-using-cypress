describe('authentication login', () => {
    it('Verify Login Successfully', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid')
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
            expect(response.body).to.have.property('msg', 'ini data anda ')
        })
    })
    it('Verify Login Failed wrong email  ', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhana@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('msg', 'User tidak ada pada database kami')
        })
    })
    it('Verify Login Failed wrong password ', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiassasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('msg', 'pasword salah')
        })
    })


})
describe('authentication chek ', () => {
    it('chek user Successfully', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid')
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
            expect(response.body).to.have.property('msg', 'ini data anda ')
        })
        cy.request('get', 'http://localhost:5000/me').then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid', 'd8da917d-5846-4e28-ab45-16d64538cf22')
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
        })
    })
    it('chek user Failed', () => {
        cy.request('get', 'http://localhost:5000/me').then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('msg', 'Mohon login ke akun Anda!')
        })
    })


})
describe('authentication logout ', () => {
    it('logout Successfully', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid')
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
            expect(response.body).to.have.property('msg', 'ini data anda ')
        })
        cy.request('delete', 'http://localhost:5000/logout').then((response) => {
            expect(response.status).to.eq(200)
        })
    })
    it('logout Failed', () => {
        cy.request('delete', 'http://localhost:5000/logout').then((response) => {
            expect(response.status).to.eq(200)
        })
    })

})