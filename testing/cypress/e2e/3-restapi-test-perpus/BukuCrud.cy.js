import { generateRandomString, generateRandomPhoneNumber, generateRandomGenre } from "./generator"
describe('CRUD USER', () => {

    it('Add buku', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
        })
        cy.request('post', 'http://localhost:5000/Buku/create', {
            isbn: `${generateRandomPhoneNumber()}`,
            judul: `${generateRandomString(7)}`,
            pengarang: `${generateRandomString(7)}`,
            genre: `${generateRandomGenre()}`,
            terbit: "2023",
            stok: 10
        }).then((response) => {
            expect(response.status).to.eq(201)
            expect(response.body).to.have.property('msg', 'data berhasil di sempan')
        })
    })
    it('Chek ALL Buku ', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
        })
        cy.request('get', 'http://localhost:5000/Buku').then((response) => {
            expect(response.status).to.eq(200)
        })
    })
    it('Chek Buku By UUID', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
        })
        cy.request('get', 'http://localhost:5000/Buku/27c2f81c-8950-4632-89d3-078f237bfe6b').then((response) => {
            expect(response.status).to.eq(200)
        })


    })


    it('update Buku By UUID', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
        })
        cy.request('patch', 'http://localhost:5000/Buku/update/27c2f81c-8950-4632-89d3-078f237bfe6b', {
            isbn: `${generateRandomPhoneNumber()}`,
            judul: `${generateRandomString()}`,
            pengarang: `${generateRandomString()}`,
            genre: `${generateRandomGenre()}`,
            terbit: "2023",
            stok: 10
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('msg', 'data buku telah terupdate')
        })


    })

    it.skip('Delete Buku By UUID', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)

        })
        cy.request('delete', 'http://localhost:5000/Buku/delete/62f68ff7-4156-4ca5-9cf3-478065b80bce').then((response) => {
            expect(response.status).to.eq(200)
        })


    })


})


