
describe('Aksi Buku', () => {
    it('Pinjam Buku admin 1x', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid', 'd8da917d-5846-4e28-ab45-16d64538cf22')
        })
        cy.wait(1000)
        cy.request('post', 'http://localhost:5000/pinjam/create', {
            BukuID: "27c2f81c-8950-4632-89d3-078f237bfe6b"
        }).then((response) => {
            expect(response.status).to.eq(200)
        })
    })
    it('Chek ALL Buku admin ', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
        })
        cy.wait(1000)
        cy.request('get', 'http://localhost:5000/Buku').then((response) => {
            expect(response.status).to.eq(200)
        })
        // disini akan menampikan buku yang dipinjam semua penguna termasuk user maupun admin
    })

    it.skip('Delete Buku By UUID', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
        })
        cy.wait(1000)
        cy.request('delete', 'http://localhost:5000/pinjam/delete/5af0651c-52c2-4d59-9e81-6d26c4d2f932').then((response) => {
            expect(response.status).to.eq(200)
        })


    })


})


