export const generateRandomString = (length) => {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export const generateRandomRole = () => {
    const roles = ['user', 'admin'];
    const randomIndex = Math.floor(Math.random() * roles.length);
    return roles[randomIndex];
}

export const generateRandomGenre = () => {
    const roles = ['sedih', 'senag', 'horor'];
    const randomIndex = Math.floor(Math.random() * roles.length);
    return roles[randomIndex];
}

export const generateRandomPhoneNumber = () => {
    const firstPart = Math.floor(Math.random() * 1000).toString().padStart(3, '0');
    const secondPart = Math.floor(Math.random() * 1000).toString().padStart(3, '0');
    const thirdPart = Math.floor(Math.random() * 10000).toString().padStart(4, '0');
    return `${firstPart}-${secondPart}-${thirdPart}`;

}