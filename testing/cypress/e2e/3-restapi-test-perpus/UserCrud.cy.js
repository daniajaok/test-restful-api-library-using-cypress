import { generateRandomString, generateRandomRole } from "./generator"

describe('CRUD USER', () => {
    it.skip('Add User', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid', 'd8da917d-5846-4e28-ab45-16d64538cf22')
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
            expect(response.body).to.have.property('msg', 'ini data anda ')
        })
        cy.request('post', 'http://localhost:5000/user/create', {
            alamat: "jawa",
            kontak: "081220000",
            name: "user2",
            email: `${generateRandomString(10)}@gmail.com`,
            password: "admiasasnsaja",
            confPassword: "admiasasnsaja",
            role: `${generateRandomRole()}`
        }).then((response) => {
            expect(response.status).to.eq(201)
            expect(response.body).to.have.property('msg', 'Registrasi sukses')
        })
    })
    it('Chek ALL User ', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid', 'd8da917d-5846-4e28-ab45-16d64538cf22')
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
            expect(response.body).to.have.property('msg', 'ini data anda ')
        })
        cy.request('get', 'http://localhost:5000/user').then((response) => {
            expect(response.status).to.eq(200)
        })
    })
    it('Chek User By UUID', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid', 'd8da917d-5846-4e28-ab45-16d64538cf22')
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
            expect(response.body).to.have.property('msg', 'ini data anda ')
        })
        cy.request('get', 'http://localhost:5000/user/d8da917d-5846-4e28-ab45-16d64538cf22').then((response) => {
            expect(response.status).to.eq(200)
        })


    })


    it('update User By UUID', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid', 'd8da917d-5846-4e28-ab45-16d64538cf22',)
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
            expect(response.body).to.have.property('msg', 'ini data anda ')
        })
        cy.request('patch', 'http://localhost:5000/user/update/1dc9c95b-9b24-4149-bb2f-1596d8a75484', {
            email: "mVJefnxwG0@gmail.com",
            alamat: "jawa",
            kontak: "081220000",
            ame: "dani 2",
            password: "admiasasnsaja",
            confPassword: "admiasasnsaja",
            role: "admin"
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('msg', 'User update sukses')
        })


    })

    it.skip('Delete User By UUID', () => {
        cy.request('POST', 'http://localhost:5000/login', {
            email: 'burhan@gmial.com',
            password: 'admiasasnsaja'
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('uuid', 'd8da917d-5846-4e28-ab45-16d64538cf22',)
            expect(response.body).to.have.property('name', 'Muhammad Ramdhani')
            expect(response.body).to.have.property('email', 'burhan@gmial.com')
            expect(response.body).to.have.property('role', 'admin')
            expect(response.body).to.have.property('msg', 'ini data anda ')
        })
        cy.request('delete', 'http://localhost:5000/user/delete/f2a94346-c786-4c4a-a2ba-0b79d34c7dbb').then((response) => {
            expect(response.status).to.eq(200)
        })


    })


})


