import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()

import argon2 from "argon2";

export const Login =async(req,res)=>{
    const user = await prisma.users.findUnique({
        where: {
            email: req.body.email// ini mengambil(req) dari data body pada html atau json
        }
    });
    
    if (!user) {
        return res.status(404).json({ msg: 'User tidak ada pada database kami' });
    }
    const match =await argon2.verify(user.password, req.body.password)//verify() berfugsi sebagai ferefikasi pakah data user.password(di database) dan req.body.password( pada reques(req) dari data body pada html atau json ) apakah sama   
    let NotLogin;
    if (!match) {
        return res.status(400).json({msg:'pasword salah'})
    }
    else{
        req.session.userId=user.uuid;
        const uuid=user.uuid
        const name=user.name
        const email=user.email
        const role =user.role
        NotLogin=user.uuid
        res.status(200).json({uuid,name,email,role,msg:'ini data anda '})
    }



}

export const Me = async (req, res) => {
    console.info(req.session.userId)
    if(!req.session.userId) {
        return res.status(401).json({msg: "Mohon login ke akun Anda!"});
    }
    const user = await prisma.users.findUnique({
            select: {
                id: false,
                uuid:true,
                name: true,
                email: true,
                password:false,
                role:true,
                createdAt:false,
                createdAt:false,
              },
            where: {
                uuid: req.session.userId
            }
        });
    if(!user) return res.status(404).json({msg: "User tidak ditemukan"}); 
    res.status(200).json (user);


    // if(!req.session.userId){
    //     return res.status(401).json({msg: "Mohon login ke akun Anda!"});
    // }
    // const user = await UserModel.findOne({
    //     attributes:['uuid','name','email','role'],
    //     where: {
    //         uuid: req.session.userId
    //     }
    // });
    // if(!user) return res.status(404).json({msg: "User tidak ditemukan"});
    // res.status(200).json(user);
}


export const logOut= async (req,res)=>{
    req.session.destroy((err)=>{
        if(err){
            return res.status(400).json({msg:"tidak dapat logout"})
        }
        else{
            res.status(200).json({msg:"anda telah keluar"})
        }
    })

}

// import UserModel from "../models/UserModel.js"; // Ini adalah pernyataan impor untuk mengimpor model UserModel dari file "../models/UserModel.js".

// import argon from "argon2"; // Ini adalah pernyataan impor untuk mengimpor modul argon2, yang digunakan untuk mengelola password dengan aman.

// export const Login = async (req, res) => {
//     const User = await UserModel.findOne({
//         where: {
//             email: req.body.email
//         }
//     }); // Ini adalah pemanggilan asinkron untuk mencari data pengguna dengan alamat email yang cocok dari model UserModel.

//     const match = await argon.verify(User.password, req.body.password); // Ini adalah pemanggilan asinkron untuk memverifikasi kata sandi yang dimasukkan dengan kata sandi pengguna yang disimpan.

//     if (!User) {
//         returnres.status(404).json({ msg: 'User tidak ada pada database kami' }); // Jika pengguna tidak ditemukan, beri respons dengan pesan kesalahan 404.
//     }

//     if (!match) {
//         return res.status(400).json({ msg: 'Password salah' }); // Jika kata sandi tidak cocok, beri respons dengan pesan kesalahan 400.
//     } else {
//         const uuid = User.uuid;
//         const name = User.name;
//         const email = User.email;
//         const role = User.role;
//         req.session.UserId = User.uuid; // Mengatur ID pengguna dalam sesi.
//         res.status(200).json(uuid, name, email, role, { msg: `Berikut data Anda` }); // Beri respons dengan data pengguna yang berhasil masuk.
//     }
// }

// export const Me = async (req, res) => {
//     if (!req.session.userId) {
//         return res.status(401).json({ msg: "Mohon login ke akun Anda!" }); // Jika tidak ada ID pengguna dalam sesi, beri respons dengan pesan kesalahan 401.
//     }

//     const user = await UserModel.findOne({
//         attributes: ['uuid', 'name', 'email', 'role'],
//         where: { uuid: req.session.userId }
//     }); // Ini adalah pemanggilan asinkron untuk mencari data pengguna berdasarkan ID pengguna dalam sesi.

//     if (!user) return res.status(404).json({ msg: "User tidak ditemukan" }); // Jika pengguna tidak ditemukan, beri respons dengan pesan kesalahan 404.
//     res.status(200).json(user); // Beri respons dengan data pengguna yang berhasil ditemukan.
// }

// export const logOut = async (req, res) => {
//     req.session.destroy((err) => {
//         if (err) {
//             return res.status(400).json({ msg: "Tidak dapat logout" }); // Jika terjadi kesalahan saat menghancurkan sesi, beri respons dengan pesan kesalahan 400.
//         } else {
//             res.status(200).json({ msg: "Anda telah keluar" }); // Beri respons bahwa pengguna telah berhasil keluar.
//         }
//     })
// }
