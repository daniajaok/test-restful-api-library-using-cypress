// import UserModel from "../models/UserModel.js";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()
import argon2 from "argon2";
// Ini adalah penggunaan ekspor fungsi getUser yang akan menangani permintaan HTTP.
export const getUser = async (req, res) => {
    try {
        // Dalam blok try, kita mencoba menjalankan beberapa operasi.
        const response = await prisma.users.findMany({
            select: {
                id: false,
                uuid: true,
                name: true,
                alamat: true,
                kontak: true,
                email: true,
                password: false,
                role: true,
                createdAt: false,
                createdAt: false,
            },
        });

        // Kode di atas melakukan pencarian (query) dalam model UserModel untuk mencari semua data pengguna.
        // attributes yang diambil adalah uuid, name, email, dan role.
        const sanitizedResponse = response.map(user => ({
            ...user,
            kontak: user.kontak.toString(),
        }));
        res.status(200).json(sanitizedResponse);
        // Jika operasi berhasil, kita mengirimkan respons dengan status kode 200 (OK) dan data pengguna yang ditemukan dalam format JSON.

    } catch (error) {
        // Jika terjadi kesalahan (error) selama eksekusi kode dalam blok try, kita akan masuk ke blok catch.
        res.status(500).json({ msg: error.message });
        // Kita mengirimkan respons dengan status kode 500 (Internal Server Error) dan pesan kesalahan yang dihasilkan.
    }
}

// Ini adalah penggunaan ekspor fungsi getUserById yang akan menangani permintaan HTTP.
export const getUserById = async (req, res) => {
    console.log(req.params.id)
    try {
        // Dalam blok try, kita mencoba menjalankan beberapa operasi.
        const response = await prisma.users.findUnique({
            select: {
                id: false,
                uuid: true,
                name: true,
                alamat: true,
                email: true,
                kontak: true,
                password: false,
                role: true,
                createdAt: false,
                createdAt: false,
            },
            where: {
                uuid: req.params.id
            }
        });

        // Kode di atas melakukan pencarian (query) dalam model UserModel untuk mencari data pengguna berdasarkan uuid yang diterima dari parameter permintaan (req.params.id).
        // attributes yang diambil adalah uuid, name, email, dan role.

        if (response) {
            // Mengonversi kontak ke String jika ada
            if (response.kontak !== null) {
                response.kontak = response.kontak.toString();
            }

            res.status(200).json(response);
        } else {
            res.status(404).json({ msg: "Data not found" });
        }
        // Jika operasi berhasil, kita mengirimkan respons dengan status kode 200 (OK) dan data pengguna yang ditemukan dalam format JSON.

    } catch (error) {
        // Jika terjadi kesalahan (error) selama eksekusi kode dalam blok try, kita akan masuk ke blok catch.
        res.status(500).json({ msg: `data ${error.message}` });
        // Kita mengirimkan respons dengan status kode 500 (Internal Server Error) dan pesan kesalahan yang dihasilkan.
    }
}

// Ini adalah penggunaan ekspor fungsi createUser yang akan menangani permintaan HTTP.
export const createUser = async (req, res) => {
    // const hashPassword = await argon2.hash('sssssasaasas');
    // await prisma.users.create({

    //     data:{
    //         name: 'passssssss',
    //         email: 'asasasasa@gmal.com',
    //         password: hashPassword,
    //         role: 'admin'
    //     } 
    //   })
    //     .then((user) => {
    //       console.log('Pengguna telah dibuat:', user);
    //       res.status(201).json({msg:'resistrasi sukses'})
    //     })
    //     .catch((error) => {
    //       console.error('Terjadi kesalahan:', error);
    //     });
    const { name, email, password, confPassword, role, alamat, kontak } = req.body;
    // const response = await UserModel.findAll({
    //     attributes: ['uuid', 'name', 'email', 'role']
    // });
    // let isEmailRegistered = false;

    // for (const user of response) {
    //     if (user.email === email) {
    //         isEmailRegistered = true;
    //     }
    // }

    // if (isEmailRegistered) {
    //     return res.status(400).json({ msg: 'Akun sudah terdaftar' });
    // }
    // Menerima data dari permintaan (request) dalam bentuk objek (req.body) yang harus mencakup name, email, password, confPassword, dan role.

    if (password != confPassword) {
        // Memeriksa apakah password dan konfirmasi password tidak cocok.
        return res.status(400).json({ msg: 'Password dan konfirmasi password tidak cocok' });
        // Jika password tidak cocok, maka mengirimkan respons dengan status kode 400 (Bad Request) bersama dengan pesan kesalahan.
    } else {
        const hashPassword = await argon2.hash(password);
        // Melakukan hashing (pengacakan) pada password menggunakan algoritma Argon2.

        try {
            await prisma.users.create({
                data: {
                    name: name,
                    email: email,
                    alamat: alamat,
                    kontak: kontak,
                    password: hashPassword,
                    role: role,
                }
            });
            // Menciptakan (create) sebuah entitas pengguna baru dengan menggunakan data yang diterima.

            res.status(201).json({ msg: 'Registrasi sukses' });
            // Jika operasi berhasil, mengirimkan respons dengan status kode 201 (Created) dan pesan sukses.

        } catch (error) {
            res.status(400).json({ msg: error.message });
            // Jika terjadi kesalahan selama pembuatan pengguna, maka mengirimkan respons dengan status kode 400 (Bad Request) bersama dengan pesan kesalahan.
        }
    }

}

//Ini adalah penggunaan ekspor fungsi updateUser yang akan menangani permintaan HTTP.
export const updateUser = async (req, res) => {
    const updateUser = await prisma.users.findUnique({
        where: {
            uuid: req.params.id
        }
    });


    // Mencari pengguna berdasarkan uuid yang diberikan dalam parameter permintaan (req.params.id).

    if (!updateUser) {
        res.status(404).json({ msg: 'User tidak ada pada database kami' });
        return;
        // Jika pengguna tidak ditemukan, maka mengirimkan respons dengan status kode 404 (Not Found) dan pesan kesalahan.
    }

    const { name, email, password, confPassword, role, alamat, kontak } = req.body;
    let hashPassword;

    if (password === "" || password === null) {
        hashPassword = updateUser.password;
    } else {
        hashPassword = await argon2.hash(password);
    }

    // Memeriksa apakah password baru telah diberikan atau tidak. Jika tidak, maka menggunakan password yang ada dalam basis data.

    if (password !== confPassword) {
        return res.status(400).json({ msg: 'Password dan konfirmasi password tidak cocok' });
        // Jika password dan konfirmasi password tidak cocok, maka mengirimkan respons dengan status kode 400 (Bad Request) bersama dengan pesan kesalahan.
    } else {
        try {
            console.info('asasassassa', req.params.id)
            await prisma.users.update({
                data: {
                    name: name,
                    email: email,
                    alamat: alamat,
                    kontak: kontak,
                    password: hashPassword,
                    role: role,
                },
                where: {
                    id: updateUser.id
                }
            });
            // Melakukan pembaruan data pengguna dengan data yang baru. Juga mengupdate password jika password baru diberikan.

            res.status(200).json({ msg: 'User update sukses' });
            // Jika operasi pembaruan berhasil, mengirimkan respons dengan status kode 200 (OK) dan pesan sukses.

        } catch (error) {
            res.status(400).json({ msg: error.message });
            // Jika terjadi kesalahan selama pembaruan pengguna, maka mengirimkan respons dengan status kode 400 (Bad Request) bersama dengan pesan kesalahan.
        }
    }
}

// Ini adalah penggunaan ekspor fungsi deleteUser  yang akan menangani permintaan HTTP.
export const deleteUser = async (req, res) => {
    const user = await prisma.users.findUnique({
        where: {
            uuid: req.params.id
        }
    });

    if (!user) return res.status(404).json({ msg: 'User tidak ada pada database kami' });
    // Jika pengguna tidak ditemukan, maka mengirimkan respons dengan status kode 404 (Not Found) dan pesan kesalahan.

    try {
        await prisma.users.delete({
            where: {
                id: user.id
            }
        });
        res.status(200).json({ msg: 'User telah dihapus dengan sukses' });
        // Jika operasi penghapusan berhasil, mengirimkan respons dengan status kode 200 (OK) dan pesan sukses.

    } catch (error) {

        res.status(400).json({ msg: error.message });
        // Jika terjadi kesalahan selama penghapusan pengguna, maka mengirimkan respons dengan status kode 400 (Bad Request) bersama dengan pesan kesalahan.
    }
}

