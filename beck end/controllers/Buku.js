// import Products from '../models/ProductModel.js'
// import UserModel from '../models/UserModel.js'
// import {Op} from "sequelize";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()
export const getBuku = async (req, res) => {
    try {
        let response;
        if (req.role === 'admin' || req.role === 'user') {

            response = await prisma.buku.findMany({
                select: {
                    uuid: true,
                    isbn: true,
                    judul: true,
                    pengarang: true,
                    genre: true,
                    terbit: true,
                    stok: true,
                    userId: true,
                    user: {
                        select: {
                            name: true,
                            email: true
                        }
                    }
                }
            });
        }
        // else {
        //     response = await prisma.buku.findMany({
        //         where: {
        //             userId: req.userId
        //         },
        //         select: {
        //             uuid: true,
        //             isbn: true,
        //             judul: true,
        //             pengarang: true,
        //             genre: true,
        //             terbit: true,
        //             stok: true,
        //             userId: true,
        //             user: {
        //                 select: {
        //                     name: true,
        //                     email: true
        //                 }
        //             }
        //         }
        //     });
        // }
        return res.status(200).json(response)
    } catch (error) {
        return res.status(500).json({ msg: `error anda ${error.message}` })
    }

}

export const getBukuById = async (req, res) => {
    //return res.status(200).json({msg: ` id anda ${req.params.id} dadadaad ${req.params.uuid}`});
    try {
        const Buku = await prisma.buku.findUnique({
            where: {
                uuid: req.params.id
            }
        });
        if (!Buku) return res.status(404).json({ msg: "Data tidak ditemukan" });
        let response;
        if (req.role === "admin") {
            response = await prisma.buku.findMany({
                where: {
                    AND: [
                        { id: Buku.id },
                        { uuid: Buku.uuid },
                    ],
                },
                select: {
                    uuid: true,
                    isbn: true,
                    judul: true,
                    pengarang: true,
                    genre: true,
                    terbit: true,
                    stok: true,
                    userId: true,
                    user: {
                        select: {
                            name: true,
                            email: true,
                            password: false,
                            role: true,
                            createdAt: false,
                            updatedAt: false,
                        }

                    },
                }
            });
        } else {
            console.log(Buku.id)
            response = await prisma.buku.findMany({
                where: {
                    AND: [
                        { id: Buku.id },
                        { uuid: Buku.uuid },
                    ],
                },
                select: {
                    uuid: true,
                    isbn: true,
                    judul: true,
                    pengarang: true,
                    genre: true,
                    terbit: true,
                    stok: true,
                    userId: true,
                    user: {
                        select: {
                            name: false,
                            email: true,
                            password: true,
                            role: true,
                            createdAt: true,
                            updatedAt: true,
                        }

                    },
                },
            });
        }
        res.status(200).json(response);
    } catch (error) {
        console.log(error.message)
        res.status(500).json({ msg: error.message });
    }
}

export const createbuku = async (req, res) => {
    const { isbn, judul, pengarang, genre, terbit, stok } = req.body

    try {
        await prisma.buku.create({
            data: {
                isbn: isbn,
                judul: judul,
                pengarang: pengarang,
                genre: genre,
                terbit: terbit,
                stok: stok,
                userId: req.userId
            }
        })
        console.info(req.userId, "anehh")
        res.status(201).json({ msg: 'data berhasil di sempan' })
    } catch (error) {
        res.status(500).json({ msg: `error anda ${error.message}` })

    }
}

export const updateBuku = async (req, res) => {
    try {

        const Buku = await prisma.buku.findUnique({
            where: {
                uuid: req.params.id
            }
        });
        if (!Buku) return res.status(404).json({ msg: "Data tidak ditemukan" });
        const { isbn, judul, pengarang, genre, terbit, stok } = req.body
        if (req.role === "admin") {
            await prisma.buku.update({
                data: {
                    isbn: isbn,
                    judul: judul,
                    pengarang: pengarang,
                    genre: genre,
                    terbit: terbit,
                    stok: stok,
                },
                where: {
                    id: Buku.id
                }
            });

        } else {
            if (req.userId !== Buku.userId) return res.status(403).json({ msg: 'akses terlalrang' })
            console.log('asasa', req.userId)
            await prisma.buku.update({
                where: {
                    uuid: req.params.id,
                    AND: [
                        { id: Buku.id },
                        { uuid: req.params.id },
                    ],

                },
                data: {
                    isbn: isbn,
                    judul: judul,
                    pengarang: pengarang,
                    genre: genre,
                    terbit: terbit,
                    stok: stok,
                    userId: req.userId
                }

            });
        }
        res.status(200).json({ msg: 'data buku telah terupdate' });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}

export const deleteBuku = async (req, res) => {
    try {
        const Buku = await prisma.buku.findUnique({
            where: {
                uuid: req.params.id
            }
        });
        if (!Buku) return res.status(404).json({ msg: "Data tidak ditemukan" });
        if (req.role === "admin") {
            await prisma.buku.delete({
                where: {
                    id: Buku.id
                }
            });
        } else {
            if (req.userId !== Buku.userId) return res.status(403).json({ msg: 'akses terlalrang' })
            await prisma.buku.delete({
                where: {
                    uuid: req.params.id,
                    AND: [
                        { id: Buku.id },
                        { uuid: req.params.id },
                    ],
                }
            });
        }
        res.status(200).json({ msg: 'data buku telah terhapus' });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
