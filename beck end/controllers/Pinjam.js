// import Products from '../models/ProductModel.js'
// import UserModel from '../models/UserModel.js'
// import {Op} from "sequelize";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()

export const getPinjam = async (req, res) => {
    console.log(req.role)
    try {
        let response;
        if (req.role === 'admin') {

            response = await prisma.pinjamBuku.findMany({
                select: {
                    BukuID: true,
                    uuid: true,
                    user: {
                        select: {
                            name: true,
                            email: true
                        }
                    }
                }
            });
        }
        else {
            response = await prisma.pinjamBuku.findMany({
                where: {
                    userId: req.userId
                },
                select: {
                    BukuID: true,
                    uuid: true,
                    user: {
                        select: {
                            name: true,
                            email: true
                        }
                    }
                }
            });
        }
        return res.status(200).json(response)
    } catch (error) {
        return res.status(500).json({ msg: `error anda ${error.message}` })
    }

}

export const createPinjam = async (req, res) => {
    const { BukuID } = req.body
    let response;
    const countBooks = await prisma.pinjamBuku.findMany({
        where: {
            userId: req.userId,
        },
    });
    const buku = await prisma.buku.findUnique({
        where: {
            uuid: BukuID,
        },
    });
    // console.log(buku)
    // return res.status(500).json({ msg: "buku tidak tersedia " })
    // if (buku === null && buku.stok <= 0) return res.status(200).json({ msg: "buku tidak tersedia " })
    if (buku === null) return res.status(200).json({ msg: "buku tidak tersedia " })
    if (buku.stok <= 0) return res.status(200).json({ msg: "buku tidak tersedia " })
    if (countBooks.length >= 2) return res.status(200).json({ msg: "Anda sudah meminjam 2 buku." })
    await prisma.buku.update({
        where: {
            uuid: BukuID,
        },
        data: {
            stok: buku.stok - 1,
        },
    });

    await prisma.pinjamBuku.create({
        data: {
            userId: req.userId,
            BukuID: BukuID,
        },
    });
    response = await prisma.pinjamBuku.findMany({
        where: {
            userId: req.userId
        },
        select: {
            BukuID: true,
            uuid: true,
            user: {
                select: {
                    name: true,
                    email: true
                }
            }
        }
    });
    return res.status(200).json(response)
    // res.status(201).json({ msg: "buku berhasil di pinjam ", response` })

}


export const deletePinjam = async (req, res) => {
    // console.log(req.params.id)
    // return res.status(404).json({ msg: `${req.params.id}` });
    try {
        const Pinjam = await prisma.pinjamBuku.findUnique({
            where: {
                uuid: req.params.id
            }
        });
        console.log(Pinjam)
        if (!Pinjam) return res.status(404).json({ msg: "Data tidak ditemukan" });
        var selisihWaktu = Math.abs(new Date() - new Date(Pinjam.createdAt));
        var selisihHari = Math.ceil(selisihWaktu / (1000 * 60 * 60 * 24));
        if (selisihHari >= 7) {
            // console.log('Anda terkena penalti karena mengembalikan buku setelah lebih dari 7 hari.');
            const data = await prisma.denda.findUnique({
                where: {
                    userId: req.userId,
                }
            })
            if (data.userId = !req.userId) {
                await prisma.denda.create({
                    data: {
                        userId: req.userId,
                        Boolean: true,
                    },
                });
            }

            const Pinjam = await prisma.pinjamBuku.findUnique({
                where: {
                    uuid: req.params.id
                }
            });
            const Buku = await prisma.buku.findUnique({
                where: {
                    uuid: Pinjam.BukuID
                }
            });
            const Pinjam2 = await prisma.pinjamBuku.findMany({
                where: {
                    BukuID: Pinjam.BukuID
                }
            });
            if (req.role === "admin") {
                // return res.status(200).json({ msg: Buku.length })
                if (Pinjam.BukuID === Buku.uuid) {
                    await prisma.buku.update({
                        where: {
                            id: Buku.id
                        },
                        data: {
                            stok: Buku.stok + Pinjam2.length
                        }

                    });
                    await prisma.pinjamBuku.delete({
                        where: {
                            id: Pinjam.id
                        }
                    })
                    return res.status(200).json({ msg: 'Anda terkena penalti karena mengembalikan buku setelah lebih dari 7 hari dan data buku telah terhapus' });
                }
                else {
                    await prisma.buku.update({
                        where: {
                            id: Buku.id
                        },
                        data: {
                            stok: Buku.stok + 1
                        }

                    });
                    await prisma.pinjamBuku.delete({
                        where: {
                            id: Pinjam.id
                        }
                    })
                    return res.status(200).json({ msg: 'Anda terkena penalti karena mengembalikan buku setelah lebih dari 7 hari dan data buku telah terhapus' });
                }
            }
            else {
                if (req.userId !== Buku.userId) return res.status(403).json({ msg: 'akses terlalrang' })
                // console.log('asasa', req.userId)
                if (Pinjam.BukuID === Buku.uuid) {
                    await prisma.buku.update({
                        where: {
                            uuid: Buku.uuid,
                            AND: [
                                { id: Buku.id },
                                { uuid: Buku.uuid },
                            ],

                        },
                        data: {
                            stok: Buku.stok + Pinjam2.length
                        }

                    });
                    await prisma.pinjamBuku.delete({
                        where: {
                            uuid: req.params.id,

                            AND: [
                                { id: Pinjam.id },
                                { uuid: req.params.id },
                            ],
                        }
                    })
                    return res.status(200).json({ msg: 'Anda terkena penalti karena mengembalikan buku setelah lebih dari 7 hari dan data buku telah terhapus' });
                }
                else {
                    await prisma.buku.update({
                        where: {
                            uuid: Buku.uuid,
                            AND: [
                                { id: Buku.id },
                                { uuid: Buku.uuid },
                            ],

                        },
                        data: {
                            stok: Buku.stok + 1
                        }

                    });
                    await prisma.pinjamBuku.delete({
                        where: {
                            id: Pinjam.id
                        }
                    })
                    return res.status(200).json({ msg: 'Anda terkena penalti karena mengembalikan buku setelah lebih dari 7 hari dan data buku telah terhapus' });
                }
            }
            // res.status(200).json({ msg: 'Anda terkena penalti karena mengembalikan buku setelah lebih dari 7 hari dan data buku telah terhapus' });
        }
        else {
            const Pinjam = await prisma.pinjamBuku.findUnique({
                where: {
                    uuid: req.params.id
                }
            });
            const Buku = await prisma.buku.findUnique({
                where: {
                    uuid: Pinjam.BukuID
                }
            });
            const Pinjam2 = await prisma.pinjamBuku.findMany({
                where: {
                    BukuID: Pinjam.BukuID
                }
            });
            if (req.role === "admin") {
                // return res.status(200).json({ msg: Buku.length })
                if (Pinjam.BukuID === Buku.uuid) {
                    await prisma.buku.update({
                        where: {
                            id: Buku.id
                        },
                        data: {
                            stok: Buku.stok + Pinjam2.length
                        }

                    });
                    await prisma.pinjamBuku.delete({
                        where: {
                            id: Pinjam.id
                        }
                    })
                    return res.status(200).json({ msg: 'data buku telah terhapus' });
                }
                else {
                    await prisma.buku.update({
                        where: {
                            id: Buku.id
                        },
                        data: {
                            stok: Buku.stok + 1
                        }

                    });
                    await prisma.pinjamBuku.delete({
                        where: {
                            id: Pinjam.id
                        }
                    })
                    return res.status(200).json({ msg: 'data buku telah terhapus' });
                }


            }
            else {
                if (req.userId !== Buku.userId) return res.status(403).json({ msg: 'akses terlalrang' })
                // console.log('asasa', req.userId)
                if (Pinjam.BukuID === Buku.uuid) {
                    await prisma.buku.update({
                        where: {
                            uuid: Buku.uuid,
                            AND: [
                                { id: Buku.id },
                                { uuid: Buku.uuid },
                            ],

                        },
                        data: {
                            stok: Buku.stok + Pinjam2.length
                        }

                    });
                    await prisma.pinjamBuku.delete({
                        where: {
                            uuid: req.params.id,

                            AND: [
                                { id: Pinjam.id },
                                { uuid: req.params.id },
                            ],
                        }
                    })
                    return res.status(200).json({ msg: 'data buku telah terhapus' });
                }
                else {
                    await prisma.buku.update({
                        where: {
                            uuid: Buku.uuid,
                            AND: [
                                { id: Buku.id },
                                { uuid: Buku.uuid },
                            ],

                        },
                        data: {
                            stok: Buku.stok + 1
                        }

                    });
                    await prisma.pinjamBuku.delete({
                        where: {
                            id: Pinjam.id
                        }
                    })
                    return res.status(200).json({ msg: 'data buku telah terhapus' });
                }
            }
            // return res.status(200).json({ msg: 'data buku telah terhapus' });
        }
    } catch (error) {
        return res.status(500).json({ msg: error.message });
    }
}
