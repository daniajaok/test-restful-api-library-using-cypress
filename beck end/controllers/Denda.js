// import Products from '../models/ProductModel.js'
// import UserModel from '../models/UserModel.js'
// import {Op} from "sequelize";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()

export const getBukuById = async (req, res) => {
    //return res.status(200).json({msg: ` id anda ${req.params.id} dadadaad ${req.params.uuid}`});
    try {
        const Buku = await prisma.buku.findUnique({
            where: {
                uuid: req.params.id
            }
        });
        if (!Buku) return res.status(404).json({ msg: "Data tidak ditemukan" });
        let response;
        if (req.role === "admin") {
            response = await prisma.buku.findMany({
                where: {
                    AND: [
                        { id: Buku.id },
                        { uuid: Buku.uuid },
                    ],
                },
                select: {
                    uuid: true,
                    isbn: true,
                    judul: true,
                    pengarang: true,
                    genre: true,
                    images: true,
                    terbit: true,
                    stok: true,
                    userId: true,
                    user: {
                        select: {
                            name: true,
                            email: true,
                            password: false,
                            role: true,
                            createdAt: false,
                            updatedAt: false,
                        }

                    },
                }
            });
        } else {
            console.log(Buku.id)
            response = await prisma.buku.findMany({
                where: {
                    AND: [
                        { id: Buku.id },
                        { uuid: Buku.uuid },
                    ],
                },
                select: {
                    uuid: true,
                    isbn: true,
                    judul: true,
                    pengarang: true,
                    genre: true,
                    images: true,
                    terbit: true,
                    stok: true,
                    userId: true,
                    user: {
                        select: {
                            name: false,
                            email: true,
                            password: true,
                            role: true,
                            createdAt: true,
                            updatedAt: true,
                        }

                    },
                },
            });
        }
        res.status(200).json(response);
    } catch (error) {
        console.log(error.message)
        res.status(500).json({ msg: error.message });
    }
}
export const getPinjam = async (req, res) => {
    try {
        let response;
        if (req.role === 'admin') {

            response = await prisma.pinjamBuku.findMany({
                select: {
                    BukuID: true,
                    uuid: true,
                    user: {
                        select: {
                            name: true,
                            email: true
                        }
                    }
                }
            });
        }
        else {
            response = await prisma.pinjamBuku.findMany({
                where: {
                    userId: req.userId
                },
                select: {
                    BukuID: true,
                    uuid: true,
                    user: {
                        select: {
                            name: true,
                            email: true
                        }
                    }
                }
            });
        }
        return res.status(200).json(response)
    } catch (error) {
        return res.status(500).json({ msg: `error anda ${error.message}` })
    }

}

export const createPinjam = async (req, res) => {
    const { BukuID } = req.body

    try {
        await prisma.pinjamBuku.create({
            data: {
                BukuID: BukuID,
                userId: req.userId
            }
        })
        console.info(res.userId, "anehh")
        res.status(201).json({ msg: 'data berhasil di sempan' })
    } catch (error) {
        res.status(500).json({ msg: `error anda ${error.message}` })

    }
}

export const updateBuku = async (req, res) => {
    try {

        const Buku = await prisma.buku.findUnique({
            where: {
                uuid: req.params.id
            }
        });
        if (!Buku) return res.status(404).json({ msg: "Data tidak ditemukan" });
        const { isbn, judul, pengarang, genre, images, terbit, stok } = req.body
        if (req.role === "admin") {
            await prisma.buku.update({
                data: {
                    isbn: isbn,
                    judul: judul,
                    pengarang: pengarang,
                    genre: genre,
                    images: images,
                    terbit: terbit,
                    stok: stok,
                },
                where: {
                    id: Buku.id
                }
            });

        } else {
            if (req.userId !== Buku.userId) return res.status(403).json({ msg: 'akses terlalrang' })
            console.log('asasa', req.userId)
            await prisma.buku.update({
                where: {
                    uuid: req.params.id,
                    AND: [
                        { id: Buku.id },
                        { uuid: req.params.id },
                    ],

                },
                data: {
                    isbn: isbn,
                    judul: judul,
                    pengarang: pengarang,
                    genre: genre,
                    images: images,
                    terbit: terbit,
                    stok: stok,
                    userId: req.userId
                }

            });
        }
        res.status(200).json({ msg: 'data produk telah terupdate' });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}

export const deletePinjam = async (req, res) => {
    try {
        const Pinjam = await prisma.pinjamBuku.findUnique({
            where: {
                uuid: req.params.id
            }
        });
        if (!Pinjam) return res.status(404).json({ msg: "Data tidak ditemukan" });
        if (true) {
            const Buku = await prisma.buku.findUnique({
                where: {
                    uuid: Pinjam.BukuID
                }
            });
            const i = 0
            if (req.role === "admin") {
                await prisma.buku.update({
                    data: {
                        stok: i++,
                    },
                    where: {
                        id: Buku.id
                    }
                });

            } else {
                if (req.userId !== Buku.userId) return res.status(403).json({ msg: 'akses terlalrang' })
                console.log('asasa', req.userId)
                await prisma.buku.update({
                    where: {
                        uuid: Buku.uuid,
                        AND: [
                            { id: Buku.id },
                            { uuid: Buku.uuid },
                        ],

                    },
                    data: {
                        stok: i++
                    }

                });
            }
        }
        if (req.role === "admin") {
            await prisma.pinjamBuku.delete({
                where: {
                    id: Pinjam.id
                }
            });
        } else {
            if (req.userId !== Pinjam.userId) return res.status(403).json({ msg: 'akses terlalrang' })
            await prisma.pinjamBuku.delete({
                where: {
                    uuid: req.params.id,
                    AND: [
                        { id: Pinjam.id },
                        { uuid: req.params.id },
                    ],
                }
            });
        }
        res.status(200).json({ msg: 'data produk telah terhapus' });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
}
