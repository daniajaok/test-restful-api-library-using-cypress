import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()

export const verifyUser = async (req, res, next) => {
    if (!req.session.userId) {
        return res.status(401).json({ msg: "Mohon login ke akun Anda!" });
    }
    let user = await prisma.users.findUnique({
        select: {
            id: true,
            uuid: true,
            name: true,
            email: true,
            password: false,
            role: true,
            createdAt: false,
            createdAt: false,
        },
        where: {
            uuid: req.session.userId
        }
    });
    if (!user) return res.status(404).json({ msg: "User tidak ditemukan" });
    req.userId = user.id
    console.log(`data id anda ${user.id} data role anda ${user.role}`)
    req.role = user.role
    next()

}
export const adminOnly = async (req, res, next) => {
    const user = await prisma.users.findUnique({
        select: {
            id: false,
            uuid: true,
            name: true,
            email: true,
            password: false,
            alamat: true,
            kontak: true,
            role: true,
            createdAt: false,
            createdAt: false,
        },
        where: {
            uuid: req.session.userId
        }
    });
    console.log('jenis', user.role)
    if (!user) return res.status(404).json({ msg: "User tidak ditemukan" });

    if (user.role !== 'admin') return res.status(403).json({ msg: 'akses terlalrang' })
    next()
}

export const notLogin = async (req, res, next) => {
    if (req.session.userId) return res.status(404).json({ msg: "silahkan logout dahulu" });
    next()
}
