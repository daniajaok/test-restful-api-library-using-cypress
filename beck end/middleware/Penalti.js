import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()

export const chekpenalti = async (req, res, next) => {

    let user = await prisma.denda.findUnique({
        where: {
            userId: req.session.ID_user
        }
    });
    console.log(user)
    if (user == null) {
        next()
    }
    else {
        var selisihWaktu = Math.abs(new Date() - new Date(user.createdAt));
        var selisihHari = Math.ceil(selisihWaktu / (1000 * 60 * 60 * 24));
        if (selisihHari >= 3) {
            await prisma.denda.delete({
                where: {
                    userId: req.session.ID_user
                }
            });
            next()
        }
        else if (user.Boolean === true) {
            return res.status(200).json({ msg: "anda tidak boleh meminjam buku selama kurang lebih 3 hari" });
        }
    }

}