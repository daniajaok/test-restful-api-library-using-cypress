import express from 'express'
import { createPinjam, getPinjam, deletePinjam } from '../controllers/Pinjam.js'
import { chekpenalti } from '../middleware/Penalti.js'
import { verifyUser } from '../middleware/AuthUser.js'
const router = express.Router()

router.get('/pinjam', verifyUser, getPinjam)
router.post('/pinjam/create', verifyUser, chekpenalti, createPinjam)//, verifyUser
router.delete('/pinjam/delete/:id', verifyUser, deletePinjam)
export default router
