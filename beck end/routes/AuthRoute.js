import express from 'express'
import {Login,logOut,Me} from '../controllers/Auth.js'
import {notLogin} from '../middleware/AuthUser.js'
const router = express.Router()

router.get('/me', Me)
router.post('/login',notLogin, Login)
router.delete('/logout', logOut)

export default router
