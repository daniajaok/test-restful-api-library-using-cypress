import express from 'express'
import { createbuku, getBuku, getBukuById, updateBuku, deleteBuku } from '../controllers/Buku.js'
// import { getProducts, createbuku, deleteProduct, updateProduct, getProductById } from '../controllers/Products.js'
import { verifyUser } from '../middleware/AuthUser.js'
const router = express.Router()

// router.get('/Product', verifyUser,getProducts)
// router.get('/Product/:id', verifyUser, getProductById)
// router.patch('/Product/update/:id', verifyUser, updateProduct)
// router.post('/Product/create', verifyUser, createProduct)//, verifyUser
// router.delete('/Product/delete/:id', verifyUser, deleteProduct)

router.get('/Buku', verifyUser, getBuku)
router.get('/Buku/:id', verifyUser, getBukuById)
router.patch('/Buku/update/:id', verifyUser, updateBuku)
router.post('/Buku/create', verifyUser, createbuku)//, verifyUser
router.delete('/Buku/delete/:id', verifyUser, deleteBuku)
export default router