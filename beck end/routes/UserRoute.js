import express from 'express'
import { getUser, getUserById, updateUser, deleteUser, createUser } from '../controllers/Users.js'
import { verifyUser, adminOnly } from '../middleware/AuthUser.js'
const router = express.Router();

router.get('/user', verifyUser, adminOnly, getUser)//
router.get('/user/:id', verifyUser, adminOnly, getUserById)//
router.patch('/user/update/:id', verifyUser, adminOnly, updateUser)//
router.post('/user/create', verifyUser, adminOnly, createUser)//
router.delete('/user/delete/:id', verifyUser, adminOnly, deleteUser)//
// router.get('/user', getUser)//
// router.get('/user/:id', getUserById)//
// router.patch('/user/update/:id', updateUser)//
// router.post('/user/create', createUser)//
// router.delete('/user/delete/:id', deleteUser)//

export default router;
