-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Mar 2024 pada 16.57
-- Versi server: 10.4.28-MariaDB
-- Versi PHP: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaanv2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id` int(11) NOT NULL,
  `uuid` varchar(191) NOT NULL,
  `isbn` varchar(1000) NOT NULL,
  `judul` varchar(1000) NOT NULL,
  `pengarang` varchar(1000) NOT NULL,
  `genre` varchar(1000) NOT NULL,
  `terbit` varchar(191) NOT NULL,
  `stok` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id`, `uuid`, `isbn`, `judul`, `pengarang`, `genre`, `terbit`, `stok`, `userId`, `createdAt`, `updatedAt`) VALUES
(2, '5af0651c-52c2-4d59-9e81-6d26c4d2f932', '456-456-12345', 'The Hobbit, or There and Back Again', 'J.R.R. Tolkien', 'logika', '2023', 7, 1, '2024-03-13 05:59:30', '2024-03-14 14:17:33'),
(3, '7facca8b-81ee-40ff-836d-299a84119b50', '789-789-12345', 'A Study in Scarlet', 'Arthur Conan Doyle', 'teknik', '2023', 10, 1, '2024-03-13 06:01:45', '2024-03-14 04:20:23'),
(4, '27c2f81c-8950-4632-89d3-078f237bfe6b', '995-840-8672', '', '', 'horor', '2023', 7, 1, '2024-03-13 06:02:24', '2024-03-14 14:13:08'),
(5, '6b0fb755-e1f6-49ad-9467-ed04d648641b', '789-789-6789', 'Harry Potter', 'J.K Rowling', 'magis', '2023', 7, 1, '2024-03-14 13:29:49', '2024-03-14 13:29:49'),
(9, '62f68ff7-4156-4ca5-9cf3-478065b80bce', '084-644-5712', 'A1w3twD', '0N0X4mB', 'senag', '2023', 10, 1, '2024-03-14 13:42:28', '2024-03-14 13:42:28'),
(10, 'ff79b489-5bf6-43b9-9210-b4152d9ab693', '621-630-7453', 'DjT9MAq', 't39vnjq', 'sedih', '2023', 10, 1, '2024-03-14 13:43:47', '2024-03-14 13:43:47'),
(11, '679ace71-029a-45bf-a29a-c978e1a793f4', '611-373-6018', 'HfXN1dF', '3wutOPc', 'horor', '2023', 10, 1, '2024-03-14 13:44:27', '2024-03-14 13:44:27'),
(12, '7109cf69-1aa8-4452-a4cb-ab2b1cb29f1c', '455-138-2391', 'NM9v5t7', 'PEULZ4T', 'horor', '2023', 10, 1, '2024-03-14 13:46:24', '2024-03-14 13:46:24'),
(13, 'fbf1018a-4e8f-4f80-aa19-3d30aae03828', '293-356-9392', 'QxnnA3R', 'PaggQr4', 'horor', '2023', 10, 1, '2024-03-14 13:46:58', '2024-03-14 13:46:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `denda`
--

CREATE TABLE `denda` (
  `id` int(11) NOT NULL,
  `uuid` varchar(191) NOT NULL,
  `userId` int(11) NOT NULL,
  `Boolean` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pinjambuku`
--

CREATE TABLE `pinjambuku` (
  `id` int(11) NOT NULL,
  `uuid` varchar(191) NOT NULL,
  `userId` int(11) NOT NULL,
  `BukuID` varchar(191) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pinjambuku`
--

INSERT INTO `pinjambuku` (`id`, `uuid`, `userId`, `BukuID`, `createdAt`, `updatedAt`) VALUES
(38, '5a633735-c9cf-4283-9209-6c03d6274bea', 1, '27c2f81c-8950-4632-89d3-078f237bfe6b', '2024-03-14 13:54:42', '2024-03-14 13:54:42'),
(39, 'b7f6c427-112f-4b7b-97b2-aec6a0823971', 1, '27c2f81c-8950-4632-89d3-078f237bfe6b', '2024-03-14 14:13:08', '2024-03-14 14:13:08'),
(40, 'c843596e-b817-4fe7-b077-826e949f7d7d', 6, '5af0651c-52c2-4d59-9e81-6d26c4d2f932', '2024-03-14 14:17:33', '2024-03-14 14:17:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `session`
--

CREATE TABLE `session` (
  `id` varchar(191) NOT NULL,
  `sid` varchar(191) NOT NULL,
  `data` varchar(191) NOT NULL,
  `expiresAt` datetime(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `session`
--

INSERT INTO `session` (`id`, `sid`, `data`, `expiresAt`) VALUES
('_eitAu3BMjprmMBvYNo7mv-zb0xOwmRx', '_eitAu3BMjprmMBvYNo7mv-zb0xOwmRx', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2024-03-15 13:32:24.790'),
('_LlWy6k3CUZkYdrXjymQwybA7m8SHxSZ', '_LlWy6k3CUZkYdrXjymQwybA7m8SHxSZ', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:44:59.735'),
('_MYvSJitjzvRGvabnL5qpFkLB8AQAyIe', '_MYvSJitjzvRGvabnL5qpFkLB8AQAyIe', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:54:24.539'),
('-B_VuEOToa3NBDI2IVcIaDdvyUlgbQqx', '-B_VuEOToa3NBDI2IVcIaDdvyUlgbQqx', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:32:47.709'),
('-m3jaq-B0rVxcBo3_FdZRqWv_j5gI8uD', '-m3jaq-B0rVxcBo3_FdZRqWv_j5gI8uD', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:25:12.076'),
('-strYBtrH8N86W5GQV359lUKMmniBsT1', '-strYBtrH8N86W5GQV359lUKMmniBsT1', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 08:17:31.643'),
('17TWoed3ExKaLAzbcFlML21G3dZwCisk', '17TWoed3ExKaLAzbcFlML21G3dZwCisk', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:53:03.028'),
('1agBTJDRvkHUARw0l8OBBymXlN3PW0R-', '1agBTJDRvkHUARw0l8OBBymXlN3PW0R-', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:59:38.517'),
('1d4T421KbaPhH8ojo7bCnTpZPlcv963W', '1d4T421KbaPhH8ojo7bCnTpZPlcv963W', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:20.396'),
('1l2ysJCl1tPfNoVA3k25NgsIrX2V9_Ub', '1l2ysJCl1tPfNoVA3k25NgsIrX2V9_Ub', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:13.941'),
('1ptI41VZRu53K78qBS1BeR3I8uQvsJzS', '1ptI41VZRu53K78qBS1BeR3I8uQvsJzS', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:40:12.751'),
('1qJuyTGaTFwT42fln80f_qsfIo_jBSj-', '1qJuyTGaTFwT42fln80f_qsfIo_jBSj-', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:13:55.849'),
('2DxJ0oOMmz2tWpWdGhZiX-nanZtK4gkG', '2DxJ0oOMmz2tWpWdGhZiX-nanZtK4gkG', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:21:49.207'),
('2NozeAP4tYUodJgVvne6J_dIl9hbMUja', '2NozeAP4tYUodJgVvne6J_dIl9hbMUja', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:59:38.786'),
('2sw5G1XncWA0up1vwvoE1XTIPE1-99_L', '2sw5G1XncWA0up1vwvoE1XTIPE1-99_L', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:05:21.295'),
('2T0To5ZCjv_AnAJdQUBEUH94JVh-UfpA', '2T0To5ZCjv_AnAJdQUBEUH94JVh-UfpA', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:47:08.698'),
('2wq4FgJ3ToI9mbn4TTkMHJ-AxAHwuV7_', '2wq4FgJ3ToI9mbn4TTkMHJ-AxAHwuV7_', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:49:31.922'),
('2ZBH0JsFCgaHi6pawEPfknSCCXJ6teIx', '2ZBH0JsFCgaHi6pawEPfknSCCXJ6teIx', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:46:25.344'),
('3bYUg8c6ydvsuodKIDt7A2pO2UoenZze', '3bYUg8c6ydvsuodKIDt7A2pO2UoenZze', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2024-03-15 08:23:10.400'),
('3QZiHGbMmNfvZGbDkC1y6IR8gkNykMHV', '3QZiHGbMmNfvZGbDkC1y6IR8gkNykMHV', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:32:24.715'),
('3ZtswsTSWU--Lr0CHwTV5nsgeR-uhlmf', '3ZtswsTSWU--Lr0CHwTV5nsgeR-uhlmf', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:37:39.842'),
('4HTWwFEai20PbadzcoulasJ0vUWy3mtM', '4HTWwFEai20PbadzcoulasJ0vUWy3mtM', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:31:55.363'),
('4mj6ptj-agES5V9C5qOmSKp6Q8bntHG7', '4mj6ptj-agES5V9C5qOmSKp6Q8bntHG7', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:05:21.550'),
('4nOpW4Gq2YQ4N2o1PrhzIbQvjpyq2QVD', '4nOpW4Gq2YQ4N2o1PrhzIbQvjpyq2QVD', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:49:32.306'),
('5vB-cGIWnE9NzYms4ObaS1jl3KX2jFfo', '5vB-cGIWnE9NzYms4ObaS1jl3KX2jFfo', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:37:52.899'),
('7_NsxuhMZKxC4zNkx2kXMBUrVRhHMQXy', '7_NsxuhMZKxC4zNkx2kXMBUrVRhHMQXy', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:46:59.443'),
('7KZ79XvVJWQQPUeBtjJtbB8HFRaU8_Zg', '7KZ79XvVJWQQPUeBtjJtbB8HFRaU8_Zg', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:57:05.644'),
('7QjZxeWa8nBEMXZWqixtMg8LqWGu6b_m', '7QjZxeWa8nBEMXZWqixtMg8LqWGu6b_m', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:46:48.486'),
('7rfdIs3ZEa1LURWvCY94cn_qpIYu5G6W', '7rfdIs3ZEa1LURWvCY94cn_qpIYu5G6W', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:47:59.628'),
('7tc0-4njz40eENYgXnrP8NhebhRnNI-F', '7tc0-4njz40eENYgXnrP8NhebhRnNI-F', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:45:00.091'),
('846-ONiaUQEKAhV_IeoUE4KP-vt4OvmT', '846-ONiaUQEKAhV_IeoUE4KP-vt4OvmT', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 08:17:11.858'),
('9ax_3uyQCAxKteD9WGBRkecSfjx5qXON', '9ax_3uyQCAxKteD9WGBRkecSfjx5qXON', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:13.709'),
('a0cyoDLvoyS14f10FwQhaWdWMnbW81jF', 'a0cyoDLvoyS14f10FwQhaWdWMnbW81jF', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2024-03-15 09:02:50.691'),
('A8Rpyevhl1MkAj65KEXuYk_4BFD3PfgG', 'A8Rpyevhl1MkAj65KEXuYk_4BFD3PfgG', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 14:51:53.810'),
('Ae7piPZLvl-HLerTreRWmnWw9gB9iG73', 'Ae7piPZLvl-HLerTreRWmnWw9gB9iG73', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:46.813'),
('AIaJEb7JhHbhBpiuuWYTaE4aD3A8Iceb', 'AIaJEb7JhHbhBpiuuWYTaE4aD3A8Iceb', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:53:03.297'),
('AKEXIYFay-C5gofmyLuEI8gmlBQyap2w', 'AKEXIYFay-C5gofmyLuEI8gmlBQyap2w', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:51:29.371'),
('aRgTSswpLqp_-TgyeO1QKf6aug6d-sap', 'aRgTSswpLqp_-TgyeO1QKf6aug6d-sap', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:53:15.820'),
('AT3mff11tMmFNlVmIB-jM-GmM7nW7pZe', 'AT3mff11tMmFNlVmIB-jM-GmM7nW7pZe', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:45:26.182'),
('BB38FznTdEQ--pwkRVDcR3DrP92fow4T', 'BB38FznTdEQ--pwkRVDcR3DrP92fow4T', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:13:55.697'),
('BRR20nFfBwck12fKqab5vBGMIKczyYdq', 'BRR20nFfBwck12fKqab5vBGMIKczyYdq', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:20.137'),
('BRrMsBZDpIF93kp9SIAtUhfsmrnEwBX7', 'BRrMsBZDpIF93kp9SIAtUhfsmrnEwBX7', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:53:16.043'),
('Bt9QBEy7JXTzJtM48vDYjiguoEJoAedV', 'Bt9QBEy7JXTzJtM48vDYjiguoEJoAedV', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:57:05.848'),
('cFq531FJFdTHbqvxz3zPSTYyk-7bJ-Z4', 'cFq531FJFdTHbqvxz3zPSTYyk-7bJ-Z4', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:50:51.627'),
('d64HoculpHfFgCh8oSuWcmlqaoPAebzw', 'd64HoculpHfFgCh8oSuWcmlqaoPAebzw', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:46:25.739'),
('dbTL3xvnztCLi5FsEShy-4Ys1HQa_swX', 'dbTL3xvnztCLi5FsEShy-4Ys1HQa_swX', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:46:58.356'),
('dH302EvxA-3LoLCx8SReOXCxPQZE_30J', 'dH302EvxA-3LoLCx8SReOXCxPQZE_30J', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:54:42.565'),
('dZncYNmh4CZX9OlwhfQvxtInJBvXuIzc', 'dZncYNmh4CZX9OlwhfQvxtInJBvXuIzc', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:41:05.464'),
('E_Vtqq31gLhVLHXAkap6ZNe70DTQvo-B', 'E_Vtqq31gLhVLHXAkap6ZNe70DTQvo-B', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:17:34.184'),
('e_WmTw8I1l2_DoYrmkIMEODtXCz1Tdyr', 'e_WmTw8I1l2_DoYrmkIMEODtXCz1Tdyr', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:51:55.792'),
('EHIGjA1N8o6jCt3eUgi0Fx5JdAxLsxGP', 'EHIGjA1N8o6jCt3eUgi0Fx5JdAxLsxGP', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:28:43.704'),
('ExK_67pEkbnhlNomtYAbLmOmOa3gg1Ii', 'ExK_67pEkbnhlNomtYAbLmOmOa3gg1Ii', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:32:02.721'),
('F17ndjk-fs0jLCMunuCgkdLjGMT1oQLy', 'F17ndjk-fs0jLCMunuCgkdLjGMT1oQLy', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:13:56.193'),
('FMirZ59Zp0lJC76QOAtK-ExOt8DYHw5x', 'FMirZ59Zp0lJC76QOAtK-ExOt8DYHw5x', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:50.222'),
('FzH0JA53lZ8KNPZCQUmRVSR2XfGZjiOQ', 'FzH0JA53lZ8KNPZCQUmRVSR2XfGZjiOQ', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:43:47.551'),
('GAriJPBu7AXPQerbI-fmBamePo2K_jL5', 'GAriJPBu7AXPQerbI-fmBamePo2K_jL5', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:25:11.440'),
('ggLgnh5mhMZDO62TlzIWwL1sSSyKydwR', 'ggLgnh5mhMZDO62TlzIWwL1sSSyKydwR', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:48:33.417'),
('gqaaejv-zSLE2WLcaA3Jop6UIxAJLV7_', 'gqaaejv-zSLE2WLcaA3Jop6UIxAJLV7_', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:51:55.216'),
('Gsdn9Sf-v6ADZHtXQt8oznzMMNr__x-Y', 'Gsdn9Sf-v6ADZHtXQt8oznzMMNr__x-Y', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:21:49.667'),
('GxNYwQZNFF17Kw-lmHkVaoROk53s_ZhA', 'GxNYwQZNFF17Kw-lmHkVaoROk53s_ZhA', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 14:51:54.958'),
('GYRfh_5lLX9801ESY6RiBqaHD-4vZAdo', 'GYRfh_5lLX9801ESY6RiBqaHD-4vZAdo', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:44:28.026'),
('hQwyh1AOl_H9U6Izr_GPmTuu3nVJUc_5', 'hQwyh1AOl_H9U6Izr_GPmTuu3nVJUc_5', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:49:18.876'),
('IG_fHkVtenflrg-QJJjCJXzczCX4qu8B', 'IG_fHkVtenflrg-QJJjCJXzczCX4qu8B', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:32:47.858'),
('IgWuHM0z25H9nAIvs0tHCkdj7-GThuuJ', 'IgWuHM0z25H9nAIvs0tHCkdj7-GThuuJ', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:49:58.474'),
('imtNBgzQB2VXNHHlWuY1J_rmA0WGKvdu', 'imtNBgzQB2VXNHHlWuY1J_rmA0WGKvdu', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 08:17:03.180'),
('inFj9J42nB0Ne4IMCG6GoUUo4zb6WIZF', 'inFj9J42nB0Ne4IMCG6GoUUo4zb6WIZF', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:50.064'),
('iNJDtAR6lj2c654WvteImYLw1B4gyHaC', 'iNJDtAR6lj2c654WvteImYLw1B4gyHaC', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:47.020'),
('IUl9PlWdHZUC6G9_9dqHcyA2Ks6hY48o', 'IUl9PlWdHZUC6G9_9dqHcyA2Ks6hY48o', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:51:29.621'),
('jaOU3JUrqsDUO0vrRCUL6nZTuixOcIpM', 'jaOU3JUrqsDUO0vrRCUL6nZTuixOcIpM', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:16:50.562'),
('jBxc3TkMUi9dATzfAG8RDpS1AAtBWEYK', 'jBxc3TkMUi9dATzfAG8RDpS1AAtBWEYK', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:37:53.051'),
('jhmzLW8o28rBbiTcvjzJLqMwV15HgfAc', 'jhmzLW8o28rBbiTcvjzJLqMwV15HgfAc', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:02:40.608'),
('jQuxBuw8pdnP51awCcaDh6zfJjkSlQLZ', 'jQuxBuw8pdnP51awCcaDh6zfJjkSlQLZ', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:54:42.360'),
('jsjaKf0-RUDqb888b__7cU4UDiNDLAcS', 'jsjaKf0-RUDqb888b__7cU4UDiNDLAcS', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:44:28.626'),
('k-zkvKOK7l-B8EHFjKr-gewUJkPWNRGw', 'k-zkvKOK7l-B8EHFjKr-gewUJkPWNRGw', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:31:23.898'),
('KBPATyAmIeBmxfeduBqCL5FMuocFEEKO', 'KBPATyAmIeBmxfeduBqCL5FMuocFEEKO', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:28:21.484'),
('kFcg-EwtmS3rn6twz5yql9RB3bOH247q', 'kFcg-EwtmS3rn6twz5yql9RB3bOH247q', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:47:08.258'),
('KhNO8xdiLCZuwvl5o_Nd2sGuDAMfytnS', 'KhNO8xdiLCZuwvl5o_Nd2sGuDAMfytnS', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:37:40.045'),
('kU3B4lbwWDoqBP-STTIdmltrm6eDUv8G', 'kU3B4lbwWDoqBP-STTIdmltrm6eDUv8G', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:54:24.895'),
('L6yIHBhupt9IExhPuUM_QtlNUJU3qMy6', 'L6yIHBhupt9IExhPuUM_QtlNUJU3qMy6', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:46:25.064'),
('ll6SOy1SY_eVzQCMKpRR18AtEm93mj3-', 'll6SOy1SY_eVzQCMKpRR18AtEm93mj3-', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:54:42.751'),
('LtqsmIE3lwge0HijBTtBFksHXKAchTPH', 'LtqsmIE3lwge0HijBTtBFksHXKAchTPH', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:16:50.411'),
('lXmZTL-W83NBTL9eINHRc8hrJOoRnGAY', 'lXmZTL-W83NBTL9eINHRc8hrJOoRnGAY', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:42:28.232'),
('mgCKpKBZIdPE_eL2s49UDy5Yl6Dd6OEG', 'mgCKpKBZIdPE_eL2s49UDy5Yl6Dd6OEG', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:46:58.986'),
('MHF1TFXXGwdLapCL2LrKKrdRJkM2vZCQ', 'MHF1TFXXGwdLapCL2LrKKrdRJkM2vZCQ', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:59:38.160'),
('mIHEwbXrME1OxqJ46LMwNrMttKejnONb', 'mIHEwbXrME1OxqJ46LMwNrMttKejnONb', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:37:53.220'),
('mtOV5YwIr9rcc9XrgBwO9ztG387FbtfQ', 'mtOV5YwIr9rcc9XrgBwO9ztG387FbtfQ', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:45:19.717'),
('mUX7uwdA6zf8mwp3LmutRLylF6WMo3aY', 'mUX7uwdA6zf8mwp3LmutRLylF6WMo3aY', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:45:19.358'),
('myVEMVrsJRGqw6JcGUuC5eP9NudzRuU7', 'myVEMVrsJRGqw6JcGUuC5eP9NudzRuU7', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:51:37.355'),
('N8qmyUmAw5HlFMBEqTrBwObq5kxkFeR3', 'N8qmyUmAw5HlFMBEqTrBwObq5kxkFeR3', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:33:38.800'),
('NQe2aotAcBwnZLcNIctbAMzH3vVUaqxA', 'NQe2aotAcBwnZLcNIctbAMzH3vVUaqxA', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 14:18:04.491'),
('OII3shFs6RSYsiQECfb-BQWZi30VlArE', 'OII3shFs6RSYsiQECfb-BQWZi30VlArE', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\"}', '2024-03-14 15:50:34.477'),
('ozsSyDmc2twSRc6HcgQ4W55962lfB79A', 'ozsSyDmc2twSRc6HcgQ4W55962lfB79A', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:47:59.917'),
('p5kvprFIdPrrxT6oBoH2PM4ml6igeej5', 'p5kvprFIdPrrxT6oBoH2PM4ml6igeej5', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:16:50.905'),
('p77Yu12sJAsg826QTl8zLx4AianGHQ-z', 'p77Yu12sJAsg826QTl8zLx4AianGHQ-z', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:24:19.411'),
('PEi4U0ZraU7OH5Q_bmay6CpyS7srMdSC', 'PEi4U0ZraU7OH5Q_bmay6CpyS7srMdSC', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 14:13:09.186'),
('pTpIVvkt7GMyZ26Ajiu9jgs--t_wZ-Kz', 'pTpIVvkt7GMyZ26Ajiu9jgs--t_wZ-Kz', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:32:46.368'),
('PVl-pq8LlX54BTB30bNORmUEDEZ5i4vs', 'PVl-pq8LlX54BTB30bNORmUEDEZ5i4vs', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:51:37.571'),
('Q6dIMHzwEsoFWxT3BhfjBYkGCU_T6inb', 'Q6dIMHzwEsoFWxT3BhfjBYkGCU_T6inb', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:03:57.342'),
('QrRkgNjWwCypkhaUjAqqnh7AgTl_ypOv', 'QrRkgNjWwCypkhaUjAqqnh7AgTl_ypOv', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:44:59.914'),
('QZx5v9zwjoSvRmiJw8-nOiqJXlB5vxKh', 'QZx5v9zwjoSvRmiJw8-nOiqJXlB5vxKh', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:16:50.222'),
('r1UX36T_3gyfo6odZkZkCkv-Ywxpb_HS', 'r1UX36T_3gyfo6odZkZkCkv-Ywxpb_HS', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:45:25.998'),
('rdJCBx6a4uGBS7AVJ7A-wPl0Tm8uR6QX', 'rdJCBx6a4uGBS7AVJ7A-wPl0Tm8uR6QX', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:26:52.873'),
('rE_E65r6D4YD0pNKE1673i461NAPjb2v', 'rE_E65r6D4YD0pNKE1673i461NAPjb2v', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:48:33.816'),
('ReD2ozXHkyRVnyUxZWnBd7wfGzhMUwpu', 'ReD2ozXHkyRVnyUxZWnBd7wfGzhMUwpu', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:02:40.859'),
('rFW4b162ZiArdh52yFMaPq0uUrK3QNwy', 'rFW4b162ZiArdh52yFMaPq0uUrK3QNwy', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:13:55.985'),
('rgMZbR75lzHQdikptpXT5i0NE-0JhnHs', 'rgMZbR75lzHQdikptpXT5i0NE-0JhnHs', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:32:03.176'),
('RpAxFRfHhkpY_zf5cakfxYveq3wAtfyT', 'RpAxFRfHhkpY_zf5cakfxYveq3wAtfyT', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:38:41.361'),
('s38LMZYlhS7g5JUvOeFg6_cj-yFCK43A', 's38LMZYlhS7g5JUvOeFg6_cj-yFCK43A', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"}}', '2024-03-15 09:02:54.159'),
('sDEnCr0OBK9lI2LzLHyD7p9THX_-k78s', 'sDEnCr0OBK9lI2LzLHyD7p9THX_-k78s', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:49:59.211'),
('slFnN3i-IkHughPZE8QBhhrUYovNvMMM', 'slFnN3i-IkHughPZE8QBhhrUYovNvMMM', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:37:33.225'),
('sw-4Cy8-UPwbpOWs2WzSOQuccImhzKIb', 'sw-4Cy8-UPwbpOWs2WzSOQuccImhzKIb', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:02:40.743'),
('Syruiu1jnaS7tytGXvopf7MoVHIJ53sp', 'Syruiu1jnaS7tytGXvopf7MoVHIJ53sp', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:51:55.480'),
('T7SqSlDRyN7InMgDFyXSpfkVhi1qAPgV', 'T7SqSlDRyN7InMgDFyXSpfkVhi1qAPgV', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:45:19.535'),
('tfcfJ1gw0BAsorXb3ottVpwQ534iteFy', 'tfcfJ1gw0BAsorXb3ottVpwQ534iteFy', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:42:28.047'),
('ThOLAwiAQk4OzcWH_HrdviE1x1eZpFzN', 'ThOLAwiAQk4OzcWH_HrdviE1x1eZpFzN', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:49:32.125'),
('TiSs8CofFJuUsp-zhfC1W1uR3uBYEgWb', 'TiSs8CofFJuUsp-zhfC1W1uR3uBYEgWb', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:38:41.548'),
('ToPFWQ59DWklY0U2hdEVr4Hh8o4XGPGl', 'ToPFWQ59DWklY0U2hdEVr4Hh8o4XGPGl', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:29:47.800'),
('TrDpTYFYAWKz2APhvcHFnTRrKH3nyAgG', 'TrDpTYFYAWKz2APhvcHFnTRrKH3nyAgG', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:17:33.892'),
('TSHcSwDbKfmL82caVy7I2VFQPTOVg0wq', 'TSHcSwDbKfmL82caVy7I2VFQPTOVg0wq', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:44:28.307'),
('uGW7LPLIxK9Kv-bFTwh06bZrb6QrHStP', 'uGW7LPLIxK9Kv-bFTwh06bZrb6QrHStP', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:49:19.105'),
('Urse40hhUfh6HqPdniMRM2GTfpxxvTmt', 'Urse40hhUfh6HqPdniMRM2GTfpxxvTmt', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:27:30.522'),
('uwziXFBudZoqdTXNp-5PSIvhhgFF0c_h', 'uwziXFBudZoqdTXNp-5PSIvhhgFF0c_h', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:37:40.212'),
('V_qYMZr83H6I5POsgPB2bJvi6OEY-yps', 'V_qYMZr83H6I5POsgPB2bJvi6OEY-yps', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:32:47.539'),
('V2nG0KNvjAND8BI97zT2KxkEUBJPYfhP', 'V2nG0KNvjAND8BI97zT2KxkEUBJPYfhP', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:40:13.064'),
('v33BaHFnSE8k8RR26sGCl_X7oEmkvjWF', 'v33BaHFnSE8k8RR26sGCl_X7oEmkvjWF', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 14:13:08.032'),
('v3rWru92gpuLLPpsEDs3s1EN7V-FITka', 'v3rWru92gpuLLPpsEDs3s1EN7V-FITka', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:51:12.241'),
('vlnQfdnSRa83XPf0XgwLf3jTWy6k4aBY', 'vlnQfdnSRa83XPf0XgwLf3jTWy6k4aBY', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:02:50.623'),
('VQPdigj-h3UCWdj1oZZmRxFbWqAj8ckx', 'VQPdigj-h3UCWdj1oZZmRxFbWqAj8ckx', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:17:33.706'),
('W-WPO5z6AWiC2aWD6ciAaiAhoOPxv1fe', 'W-WPO5z6AWiC2aWD6ciAaiAhoOPxv1fe', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 10:45:00.308'),
('W2iCI2Hn03m-PwQQNcSV8HN-gi-YQe6w', 'W2iCI2Hn03m-PwQQNcSV8HN-gi-YQe6w', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 08:23:10.327'),
('W4W3i0jy37OSNMjKtl4ac5H6JFYzqhrv', 'W4W3i0jy37OSNMjKtl4ac5H6JFYzqhrv', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:42:03.448'),
('WGxFCc4CAEsPOzwl0w2McSzFEtPYMOQF', 'WGxFCc4CAEsPOzwl0w2McSzFEtPYMOQF', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:43:48.039'),
('wwheYbXC2IIw1NOz471oMkAyAuo69TJf', 'wwheYbXC2IIw1NOz471oMkAyAuo69TJf', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:50:51.980'),
('x1lquDDakP7HidRojibzEDPPDoAh8Pw0', 'x1lquDDakP7HidRojibzEDPPDoAh8Pw0', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:52:53.205'),
('X3tPP4ETTX-VNemYY7EIPbA8BFiijyPB', 'X3tPP4ETTX-VNemYY7EIPbA8BFiijyPB', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 12:32:02.887'),
('X6BsuPMTE8ktsEwNT1HjnTm_KJWpgEFH', 'X6BsuPMTE8ktsEwNT1HjnTm_KJWpgEFH', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:46:24.808'),
('xbeUw1TdZ_9RzYInTI5X02-H6Ry3Z6jx', 'xbeUw1TdZ_9RzYInTI5X02-H6Ry3Z6jx', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:02:54.106'),
('xzIneSSBa0GGRhvCFKYhwZ-n3s3OleTb', 'xzIneSSBa0GGRhvCFKYhwZ-n3s3OleTb', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:49:58.829'),
('Y_Im6gfxng7e0S9oyVj_l3UX8qEN1gY-', 'Y_Im6gfxng7e0S9oyVj_l3UX8qEN1gY-', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:43:47.839'),
('y0mDSH5NQ6m2rQ8OhNbGuJcix1nu7ujO', 'y0mDSH5NQ6m2rQ8OhNbGuJcix1nu7ujO', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\"}', '2024-03-15 04:20:56.512'),
('Y4aGhOENyC2tMRFpGYe7qyE-mk-71d5G', 'Y4aGhOENyC2tMRFpGYe7qyE-mk-71d5G', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:25:11.644'),
('YK4DYO2l_gRTopwX-Go7kQJkLoYMiF4s', 'YK4DYO2l_gRTopwX-Go7kQJkLoYMiF4s', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:21:49.441'),
('yXz0VVqKYfmwisKGRn6lPpyKz271xJNu', 'yXz0VVqKYfmwisKGRn6lPpyKz271xJNu', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:46:48.651'),
('Z3VxzaHFaI40X_214g3kMjstiyPNIOIP', 'Z3VxzaHFaI40X_214g3kMjstiyPNIOIP', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:29:16.995'),
('Z7swlZ4sntuAflIjoR_g3DEyGZ0JPECu', 'Z7swlZ4sntuAflIjoR_g3DEyGZ0JPECu', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 09:51:12.452'),
('zbeRCiltj9Fj1uxRU-6Os65WiSbPu5cA', 'zbeRCiltj9Fj1uxRU-6Os65WiSbPu5cA', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:47:08.420'),
('zYv0K2uLkycfOBDsiKZH1pFYFi6rkRVv', 'zYv0K2uLkycfOBDsiKZH1pFYFi6rkRVv', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:25:11.839'),
('ZyzVIMR-BZC5FD2NTT5BjYxkjDrtW7n4', 'ZyzVIMR-BZC5FD2NTT5BjYxkjDrtW7n4', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:44:27.737'),
('ZzPp2VxxNC1eOK_O59geb2ZzY0WnOJHb', 'ZzPp2VxxNC1eOK_O59geb2ZzY0WnOJHb', '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"secure\":false,\"httpOnly\":true,\"path\":\"/\"},\"userId\":\"d8da917d-5846-4e28-ab45-16d64538cf22\",\"ID_user\":1}', '2024-03-15 13:46:58.685');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `uuid` varchar(191) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `alamat` varchar(1000) NOT NULL,
  `kontak` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(191) NOT NULL,
  `role` varchar(7) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `uuid`, `name`, `alamat`, `kontak`, `email`, `password`, `role`, `createdAt`, `updatedAt`) VALUES
(1, 'd8da917d-5846-4e28-ab45-16d64538cf22', 'Muhammad Ramdhani', 'sunja', 81222, 'burhan@gmial.com', '$argon2id$v=19$m=65536,t=3,p=4$Z6Zv+PXpeEhcGt5l70UvVw$R/OqBzbbBs5dZ9mRu7a6HK9F6Cei9uQCGegAKuvHAVQ', 'admin', '2024-03-13 02:10:25', '2024-03-13 02:10:25'),
(6, '95cab60b-5c72-4a0b-a9d6-cb535ef2858b', 'user1', 'jawa', 81220000, 'burhan2@gmial.com', '$argon2id$v=19$m=65536,t=3,p=4$FZPiXXsYhZAWhU6KxP6ANQ$fN5dCNxToDOfdyTiZLRTM7BxHDLRBir4qnv/vBRK/yU', 'user', '2024-03-14 06:20:56', '2024-03-14 06:20:56'),
(8, 'dd0a227a-375c-4469-b293-3a4d87ca7899', 'user1', 'jawa', 81220000, 'burhan3@gmial.com', '$argon2id$v=19$m=65536,t=3,p=4$a9Ny2/QhRJ5M+AuXDo09SQ$jJhM+8DPqQT9J9V1yoOH4vR/qYjet0ZM9Gjy3i3p6v8', 'user', '2024-03-14 09:30:14', '2024-03-14 09:30:14'),
(9, '6c219644-8897-45d8-8a87-2895cc587b23', 'user2', 'jawa', 81220000, 'wqL0RKejG6@gmail.com', '$argon2id$v=19$m=65536,t=3,p=4$Pp75pbSLPoAZ1X+hBwjl4g$0DmreU7RHpqHvjPi54GRW/ILm32j/k1vTpzp/m68R78', 'admin', '2024-03-14 09:53:03', '2024-03-14 09:53:03'),
(10, '1dc9c95b-9b24-4149-bb2f-1596d8a75484', 'dani 2', 'jawa', 81220000, 'mVJefnxwG0@gmail.com', '$argon2id$v=19$m=65536,t=3,p=4$aP8nGKw4SpjkYGYFmw4cbg$KnLMfxUr+6NRQgiT3VV6njEC+OAIILd/iQxeQqcSK3E', 'admin', '2024-03-14 09:53:15', '2024-03-14 12:32:03'),
(11, '26317643-b0d3-4125-aac3-2120272a929b', 'user2', 'jawa', 81220000, 'UKGs5fQ4no@gmail.com', '$argon2id$v=19$m=65536,t=3,p=4$jwQSWxDQAv4uOB8QLnxsXg$zq4+jrOGhrTCsj0yTNrVjfVNUKcN6ehwmmjn+S0KOww', 'admin', '2024-03-14 09:57:05', '2024-03-14 09:57:05'),
(12, 'f2a94346-c786-4c4a-a2ba-0b79d34c7dbb', 'user2', 'jawa', 81220000, 'vdCjj84CbV@gmail.com', '$argon2id$v=19$m=65536,t=3,p=4$EN89HVvb/PjBETHRsSkChg$PDuTd5vHHhi4Xsp0SRJQji8IW/VcepUGBZdZFO6Vqhc', 'admin', '2024-03-14 09:59:38', '2024-03-14 09:59:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `_prisma_migrations`
--

CREATE TABLE `_prisma_migrations` (
  `id` varchar(36) NOT NULL,
  `checksum` varchar(64) NOT NULL,
  `finished_at` datetime(3) DEFAULT NULL,
  `migration_name` varchar(255) NOT NULL,
  `logs` text DEFAULT NULL,
  `rolled_back_at` datetime(3) DEFAULT NULL,
  `started_at` datetime(3) NOT NULL DEFAULT current_timestamp(3),
  `applied_steps_count` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `_prisma_migrations`
--

INSERT INTO `_prisma_migrations` (`id`, `checksum`, `finished_at`, `migration_name`, `logs`, `rolled_back_at`, `started_at`, `applied_steps_count`) VALUES
('13ea1554-ca96-4c81-bef8-46cb56f4697c', 'aaf4589d980cbe2b257150c796a418478b2eaf17606e662865725e8cb431ec5a', '2024-03-13 02:07:29.346', '20240313020507_v1', NULL, NULL, '2024-03-13 02:07:29.214', 1),
('4f215f24-50e4-427e-8b47-3a4a26052af1', '88e0b75b3c29d11bbcf921a0dea7e9fe2835d1639454c26f36a3a9cab4f69341', '2024-03-13 02:07:34.436', '20240313020734_v1', NULL, NULL, '2024-03-13 02:07:34.427', 1),
('6a5c5fda-d112-4d05-9688-0bc7c92547f1', '2bafdbd820123a8e5c3daa56e815813fdf1a0bfcb36b385cbe63e30c4376a1e0', '2024-03-13 10:59:51.608', '20240313105951_v2', NULL, NULL, '2024-03-13 10:59:51.555', 1),
('8bebad62-976c-4b6e-98fb-aadcb64863c2', '52fd4e58865977c6b0b147c9a02813e0cea32f1a8532908c5fb3ab05cf364400', '2024-03-14 05:42:37.278', '20240314054237_', NULL, NULL, '2024-03-14 05:42:37.256', 1),
('b12e0080-cb27-4db7-a16c-d79ec883de9e', '01d8e404d2197b04b2f71264dbf1c12ecea8181b1e2dd21c440bb1194ef66cf3', '2024-03-14 04:12:54.145', '20240314041254_v3', NULL, NULL, '2024-03-14 04:12:54.112', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Buku_uuid_key` (`uuid`),
  ADD KEY `Buku_userId_fkey` (`userId`);

--
-- Indeks untuk tabel `denda`
--
ALTER TABLE `denda`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Denda_uuid_key` (`uuid`),
  ADD UNIQUE KEY `Denda_userId_key` (`userId`);

--
-- Indeks untuk tabel `pinjambuku`
--
ALTER TABLE `pinjambuku`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `PinjamBuku_uuid_key` (`uuid`),
  ADD KEY `PinjamBuku_userId_fkey` (`userId`),
  ADD KEY `PinjamBuku_BukuID_fkey` (`BukuID`);

--
-- Indeks untuk tabel `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Session_sid_key` (`sid`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Users_uuid_key` (`uuid`),
  ADD UNIQUE KEY `Users_email_key` (`email`);

--
-- Indeks untuk tabel `_prisma_migrations`
--
ALTER TABLE `_prisma_migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `buku`
--
ALTER TABLE `buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `denda`
--
ALTER TABLE `denda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pinjambuku`
--
ALTER TABLE `pinjambuku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD CONSTRAINT `Buku_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `denda`
--
ALTER TABLE `denda`
  ADD CONSTRAINT `Denda_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pinjambuku`
--
ALTER TABLE `pinjambuku`
  ADD CONSTRAINT `PinjamBuku_BukuID_fkey` FOREIGN KEY (`BukuID`) REFERENCES `buku` (`uuid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `PinjamBuku_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
