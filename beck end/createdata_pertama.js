import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
import argon2 from "argon2";

const createData = async (name, email, alamat, kontak, password, role) => {
  const hashedPassword = await argon2.hash(password);
  await prisma.users.create({
    data: {
      name: name,
      email: email,
      alamat: alamat,
      kontak: kontak,
      role: role,
      name: name,
      email: email,
      password: hashedPassword,// Melakukan hashing (pengacakan) pada password menggunakan algoritma Argon2.
    },
  });
};

console.log(createData("Muhammad Ramdhani", "burhan@gmial.com", "sunja", "081222", "admiasasnsaja", "admin"));
