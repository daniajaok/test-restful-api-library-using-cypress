import express from 'express'; // Ini adalah pernyataan impor untuk mengimpor modul Express. Modul ini digunakan untuk membuat aplikasi server HTTP.
import cors from 'cors'; // Ini adalah pernyataan impor untuk mengimpor modul CORS (Cross-Origin Resource Sharing). Modul ini digunakan untuk mengizinkan permintaan lintas domain (cross-origin requests).
import session from 'express-session'; // Ini adalah pernyataan impor untuk mengimpor modul express-session. Modul ini digunakan untuk mengelola sesi pengguna.
import dotenv from 'dotenv'; // Ini adalah pernyataan impor untuk mengimpor modul dotenv, yang digunakan untuk mengelola variabel lingkungan.
// import db from './config/Database.js'; // Ini adalah pernyataan impor untuk mengimpor konfigurasi basis data dari direktori "config" dan digunakan untuk berinteraksi dengan basis data.
import UserRoute from './routes/UserRoute.js'; // Ini adalah pernyataan impor untuk mengimpor rute (route) pengguna dari direktori "routes".
import BukuRoute from './routes/BukuRoute.js'; // Ini adalah pernyataan impor untuk mengimpor rute (route) produk dari direktori "routes".
import AuthRoute from './routes/AuthRoute.js'
import Pinjam from './routes/PinjamRoute.js'
import bodyParser from 'body-parser'
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swaggera.json' assert { type: 'json' };
import { PrismaSessionStore } from '@quixo3/prisma-session-store';
import { PrismaClient } from '@prisma/client';
dotenv.config(); // Ini adalah panggilan untuk mengimpor dan mengkonfigurasi modul dotenv. Modul ini digunakan untuk mengatur variabel lingkungan.

const app = express(); // Ini membuat aplikasi Express yang akan digunakan sebagai server HTTP.
// (async()=>{
//     await db.sync(); // Ini adalah blok async/await untuk menunggu hingga basis data (db) disinkronkan sebelum aplikasi dimulai.
// })();
// const sessionStore=SequelizeStore(session.Store)// Membuat instance dari SequelizeStore dengan menggunakan session.Store sebagai model penyimpanan sesi.

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
  cookie: {
    maxAge: 7 * 24 * 60 * 60 * 1000 // ms
  },
  secret: process.env.SESS_SECRET, // Mengatur kunci rahasia sesi dengan mengambil nilainya dari variabel lingkungan SESS_SECRET.
  resave: false, // Mengatur opsi resave sesi menjadi false, yang berarti sesi tidak akan disimpan ulang secara otomatis.
  saveUninitialized: true, // Mengatur opsi saveUninitialized sesi menjadi true, yang berarti sesi akan disimpan bahkan jika tidak ada data yang disimpan di dalamnya.
  //store:Store,// Mengatur penyimpanan sesi untuk menggunakan SequelizeStore yang telah dibuat sebelumnya.
  store: new PrismaSessionStore(new PrismaClient(),
    {
      checkPeriod: 2 * 60 * 1000,  //ms
      dbRecordIdIsSessionId: true,
      dbRecordIdFunction: undefined,
    }),
  cookie: {
    secure: 'auto', // Mengatur opsi cookie sesi, dengan pengaturan "secure" ke "auto", yang akan menyesuaikan otomatis apakah menggunakan HTTPS atau HTTP tergantung pada lingkungan.
  },
}));

app.use(cors({
  credentials: true, // Mengatur opsi credentials CORS menjadi true, yang memungkinkan pengiriman kredensial (misalnya, cookie) dalam permintaan lintas domain.
  origin: 'http://localhost:3000', // Mengatur origin yang diizinkan untuk permintaan lintas domain, dalam hal ini adalah http://localhost:3000.
}));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(express.json()); // Menggunakan middleware express.json() untuk mengurai data permintaan HTTP dengan format JSON.
app.use(UserRoute); // Menggunakan rute (route) pengguna yang telah diimpor sebelumnya.
app.use(BukuRoute); // Menggunakan rute (route) produk yang telah diimpor sebelumnya.
app.use(Pinjam);
app.use(AuthRoute)// Menggunakan rute (route) AuthRoute yang telah diimpor sebelumnya. Ini digunakan untuk menangani rute otorisasi atau autentikasi.
// Store.sync()// Menjalankan Migrations untuk sesi. Ini menyinkronkan konfigurasi sesi dengan basis data sebelum aplikasi dimulai.
app.listen(process.env.APP_PORT, () => {
  console.info(`server sedang berjalan ${process.env.APP_PORT}`); // Memulai server Express dan mendengarkan pada port yang diambil dari variabel lingkungan APP_PORT. Ketika server berjalan, pesan "server sedang berjalan" akan ditampilkan di konsol.
});