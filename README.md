# Unit Testing Restful Api Express JS Perpustakaan MySQL

#### Mudah Banget! Testing aplikasi mengunkan cypress, gini caranya:

## Persyaratan

1. Gunakan NodeJs versi v16.20.2 atau rekomendasi node versi diatasnya.
2. Install nodemon secara global dengan command berikut:
   ```
   npm install nodemon -g
   ```
3. Pastikan konfigurasi database anda sudah benar, dan pastikan database anda sudah di running.


## Cara Menjalankan Unit Testing Restful Mengunkan Cypress 
1. Pada API unit testing devolper mengunkan cypress
2. Masuk ke dalam folder testing ini.
3. Ketikkan perintah `npm install` atau `yarn install`.
4. jika ada inggin menjalankan cypres ` npm test `
5. jalankan API unit testing pada path ` test-restful-api-library-using-cypress\testing\cypress\e2e\3-restapi-test-perpus ` di dalam folder tersebut terdapat beberpa unit testing

## Cara instalasi aplikasi
1. Clone repo ini.
2. Masuk ke dalam folder beck end ini.
3. Ketikkan perintah `npm install` atau `yarn install`.
4. Pastikan anda sudah menginstall mysql dan membuatlah data base bernama perpustakaanv2
5. rubah nama foler `konfigurasi_env.txt` menjadi `.env` .
6. Buat migrasi dari skema Prisma degan menjalankan `npx prisma migrate dev`.
7. jalankan `node createdata_pertama.js`
8. ketikkan perintah `npm start` atau `yarn start` untuk menjalankan project.
9. sekarang anda dapat menjalankan api pertama nada dibawah ini dan jika anda ingin mencoba API Documentation lainnya yang tersedia pada file Beck End Perpus.postman_collection.json dapat dibuka mengunkan postman

#### login admin

```http
  POST http://localhost:5000/login
```

| Parameter  | Type     | Description                      |
| :--------- | :------- | :--------------------------------|
| `email`    | `string` | email default `burhan@gmial.com` |
| `password` | `string` | password default `admiasasnsaja` |


